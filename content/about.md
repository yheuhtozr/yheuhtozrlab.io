+++
title = 'このサイトについて'
draft = false
+++

## 内容の利用について

本サイトの内容は、特筆がない限り [CC 表示 4.0 ライセンス](https://creativecommons.org/licenses/by/4.0/legalcode.ja)で提供します。  
お問い合わせは作者 Twitter または [GitLab リポジトリ](https://gitlab.com/yheuhtozr/yheuhtozr.gitlab.io)までお願いします。

### 素材

{{% credit %}}

## 関連リンク