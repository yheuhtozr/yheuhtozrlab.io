+++
title = '形態素と語'
description = '''
形態論で取り扱う形態素の種類とそれぞれの性質について述べます。なおフュトルには内容語の意味による区分はほぼ存在しません。
'''
draft = false
+++

## 語の構造

フュトルでは一つ以上の音母からなる形態素を核として、<dfn>語</dfn>が形成されます。
語はそれ自身のパラダイムと、それに対応する形態変化を有します。
形態変化は語の核となる形態（<dfn>語幹</dfn>）をベースとして、以下の手段によって示されます。

転回
: 語幹の音母列を交替させる（**音位転換**）

修飾
: 語幹の区間に構成される音に変更を加える

添加
: 語幹の前後に音母を持つ接辞を加える

なお、順番としては、まず転回を行って語幹の音母の並びを確定させたのち、その音母列に対して残りの操作を加えます。
多くの場合、語形変化は転回と修飾のみによって担われ、添加はそれが何らかの理由で不十分な場合の補助的手段となっています。

## 品詞分類

フュトルの品詞（実現形に音母を持つ形態素）は以下の通りに分類されます。

1. 自立語（自由形態素）
    - 冠詞／補冠詞
    - 短詞
      - 照応子
      - 実現子
        - _静的実現子_
        - 動的実現子
      - 遷延子／補遷延子
      - 転換子
    - _実詞_
      - 構造詞
      - 固結詞
1. 接辞
    - 接合子
    - 補充子
1. 接語
    - 束縛子

意味的に分類すると、*斜体*になった静的実現子および実詞が<dfn>内容的形態素</dfn>、それ以外がすべて<dfn>機能的形態素</dfn>にあたります。

<dfn>語</dfn>は 1 つの自立語と、2 個までの接辞、1 個までの接語から構成されます。
接語に分類される束縛子は、統語上は単独の語に相当する機能を有しますが、形態上は拘束形態素であり、しかもそれ自体で接辞を取ることがありません。
一語の中での形態素の順序は、必ず以下のようになります。

    (補充子) + (接合子) + 自立語 + (束縛子)

実詞・短詞・冠詞は形態音素の長さに結びついた区分です。
冠詞は 1 音母、短詞は 2 音母、実詞はそれ以上の長さの音形になります。
この違いのために語形変化の様式が異なります。

固結詞と接辞・接語は**語形変化を起こしません**。
固結詞は内容語でありながら語形変化をせず、接辞や接語をとることもないため、ほとんどの場合はそれを補う句を形成して用いられます。

## 語の音韻論

### 束縛子

束縛子は自立語に後続して 1 つの語の最後に置かれますが、束縛子と自立語の形態素境界では特別な構成規則があります。

自立語の音母のみからなる最後の区間が母音区間である場合、続く束縛子との間に構成される子音区間は**重子音**となります。
ちなみに、構造語ではこの場合以外に重子音が構成されることはありません。

（例）

### 語と音節

語を音素に構成する場合、**その語に属する音母間に構成された最初の母音区間を含む音節**がその語の<dfn>第一音節</dfn>となります。
そして、**その語**（束縛子を除く）**に属する音母が含まれる最後の母音区間を含む音節**がその語の<dfn>最終音節</dfn>となります。
つまり、語と語が隣接する場合、前後の語に挟まれた区間が母音区間であれば前の語の音節の一部となり、子音区間であれば後の語の音節の一部をなすこととなります。

（例）

### アクセント型

実詞を語幹とする語は特定のアクセントパターンを持ちます。

**構造詞**は最終母音区間の高さを**高く**、（それと区別される）第一母音区間があれば**低く**します。
**固結詞**は第一母音区間を**高く**します。

それ以外の母音区間、およびそれ以外の自立語については特別なアクセント型を持ちません（すべて**中**に相当）。
接辞は語の中で独立したアクセントを持たず、自立語のアクセントに依存します。
ただし、束縛子は語幹の母音区間の計算には関わりません。つまり束縛子が母音区間を持つ場合、それが語幹の音母を含んでいればアクセント型の影響を受け、そうでなければ影響は受けません。

## 語の分かち書き

ラテン文字転写では、文頭から順に語の間をスペースで分かち書きします。
この時、一つの語の音母から始まる区間までを綴り上の語形に入れ、その次の区間から次の語として綴ります。

（例）