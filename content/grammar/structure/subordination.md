+++
title = '冠詞の率いる句'
description = '''
構文を句として埋め込む働きをする冠詞句の性質について説明します。
'''
draft = false
+++

## 冠詞句の構造

フュトルにおける冠詞および補冠詞は補文マーカーです。
冠詞句は、冠詞から始まり、補冠詞で終わる構文で、中に句以上の任意の大きさの内容を含むことができます。
冠詞句は通常の句と同様に格や制といったパラダイムを持ち、その標示は先頭の冠詞が担います。
状況によっては補冠詞を省略することができます。

## 冠詞句と音韻上の節

冠詞の後、および補冠詞の前で音韻上の節が切断されます。
通常、音素の構成は節単位で行われ、その中では連続した音母列とみなされますが、冠詞句がある場合はその部分で途絶えます。
したがって、構成は`［節頭――冠詞］` `［冠詞句の内容］` `［補冠詞――節末］`で節が分かれているかのように別々に行われます。
節内に冠詞句が複数ある場合、また冠詞句の中に冠詞句が含まれている場合も都度切断されます。

**補冠詞が省略されたとしても、音韻上の節の切れ目は維持**されます。

## 固結詞を従える冠詞句

<b>固結詞</b>は内容語でありながら形態変化をしません。
そのため変化形を示すために冠詞句を形成します。これを特に<dfn>固結句</dfn>と呼びます。

固結句の形成は形態変化を補うためなので、当該単語が**基本形の時には冠詞を省略することができます**。
ただし固結句自体は存在しているとみなされるので、**冠詞を省略しても音韻上の節の切れ目は残ります**。

## 補冠詞の省略

（まだ）
