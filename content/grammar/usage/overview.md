+++
title = '語法概説'
description = '''
フュトルを使った言語表現において、各文法要素がどう意味に寄与するかの枠組みを解説します。
'''
date = 2020-08-22
draft = false
+++

## 節の意味について

### 節の機能

一般に節は一つの出来事あるいは現象を記述するものと説明できます。
フュトルにおいてはこの「一つ」の大きさは比較的限られており、（比較対象がないのであいまいですが）概ね均質な一動作程度の粒度となります。日本語で想像される一述語で表せる範囲のものは、複数の節の組み合わせであることが多いです。
他の言語において文の中で副詞的な意味を持つ内容は単独の節をなすことが多いです。

### 節の叙述様式

一つの節を意味的にとらえると、以下の諸要素を述べる構造になっています。

実体
: 出来事に参与する物や事柄

配置
: 出来事に関わる実体どうしの関係

定位
: 出来事を規定する環境

関係
: 他の節との対応関係

<b>実体</b>は一般にいう項（役割を持ちうる語）が保持する内容に相当しますが、述語にあたるものもここに入ります。他の言語と対照させた時に、述語にあたるものが一つの語として現れているように見えるものと、完全に節の形式（文型）が受け持っているように見えるものがあります。
実体は各種の語によって項の形で表されます。  
<b>配置</b>は実体どうしの関係を指します。一般にいう格や文法関係相当の機能を持ちますが、フュトルでは取りうる組み合わせは節の種類によって大きく制約されています。
配置は文型、および（当然ながら）それに対応する各項の変化によって表されます。  
<b>定位</b>は、出来事の背景情報を提供する要素です。時・人称・法・空間（位置・方位）など多様な内容を表し、節を支える文法機能を担います。
定位は項の中に混在する語によって表されます。特に短詞のうち実現子によって示されることが多いです。  
<b>関係</b>は他の節との関わりです。フュトルでは一つの節の表現力が限られている分、節と節を結びつけることで他の言語の一文に相当する内容を表すことが多いです。そのため関係は一般にいう指示詞、代名詞やピボット、一致のような役割を果たし、テキストを結束させます。
関係は接語・短詞によって標示されます。

これらの要素は意味的な分類であり、単一の語が複数の役割を兼ねることがあります。
実際に何らかの事態をフュトルで言い表す際は、この枠組みに則って表現されます。

### 節の連結様式

関係によって結ばれている節どうしは連結した一つの「文」とみなすことができます。
当然、（他の言語と同じく）発話の完結した単位としての文と一致するとは限らないですが、ともかくそのように関連づけられた意味単位として想定することができます。

照応的連結
: 束縛子によって語を指定し、後の節で照応子によって指示することで生じる関係

実現子間連結
: 同一の実現子を共有する節の間に生じる関係

遷延的連結
: （補）遷延子によって直前もしくは直後の節と結ばれる関係

<b>照応的連結</b>はいわゆる指示詞的な意味合いが強く、節と節の連結を直接意味するものではありませんが、これによって事実上連続した動作を表現することが多くあります。  
<b>実現子間連結</b>は、実現子の一致によって複数の節の定位の同一性を導くことにより、事態の近接性や、並行性を示唆するものであり、連結の最も典型的な手段と言えます。  
<b>遷延的連結</b>は、（補）遷延子が持つ機能によって、前後の節を埋め込むことです。これは事実上 1 つの節と同じとみなされますが、応用として接続詞に似た節を作ることがあります。

## 語および句の意味について

独自の機能がある連合句を除けば、語と句は表裏一体ですが、フュトルの文法構造上、形態的形式と意味論が完全には対応していないため、句のレベルの用語で説明します。

### 句の文法範疇と意味

### 複合句の意味

