#!/bin/bash

hugo --cleanDestinationDir -b https://yheuhtozr.gitlab.io/

WIP=`git stash`

git stash -a -- public/*

git checkout deployment
rm -rf ./public
git stash pop
git add public
TIME=`date -u '+%F %T %Z'`
git commit -m "ビルド $TIME"
git push -u origin deployment

git checkout main

if [ "A${WIP}Z" != "ANo local changes to saveZ" ]; then
  git stash pop
fi
chmod 755 deploy.sh

echo "Run deployment - $TIME."
